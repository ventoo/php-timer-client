#!/usr/bin/env php
<?php

require __DIR__ . '/../vendor/autoload.php';
$container = require __DIR__ . '/container.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;

$application = new Application();
$taggedServices = $container->findTaggedServiceIds(Command::class);
foreach ($taggedServices as $id => $tags) {
    $application->add($container->get($id));
}
$application->run();
