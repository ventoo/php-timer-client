<?php

namespace Test\Timer;

class TimerTask
{
    private $id;

    private $task;

    private $input;

    public function __construct(TaskId $id, TaskName $task, TaskInput $input)
    {
        $this->id = $id;
        $this->task = $task;
        $this->input = $input;
    }

    public function id(): TaskId
    {
        return $this->id;
    }

    public function task(): TaskName
    {
        return $this->task;
    }

    public function input(): TaskInput
    {
        return $this->input;
    }
}
