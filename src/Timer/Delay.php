<?php

namespace Test\Timer;

class Delay
{
    private $delay;

    public function __construct(int $delay)
    {
        $this->setDelay($delay);
    }

    public function delay(): int
    {
        return $this->delay;
    }

    private function setDelay(int $delay): void
    {
        $this->delay = $delay;
    }
}
