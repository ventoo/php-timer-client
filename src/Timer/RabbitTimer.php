<?php

namespace Test\Timer;

use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;

class RabbitTimer implements Timer
{
    private $connection;

    private $exchange;

    private $channel;

    public function __construct(AbstractConnection $connection, string $exchange)
    {
        $this->connection = $connection;
        $this->setExchange($exchange);
    }

    public function schedule(TimerTask $task, Delay $delay)
    {
        if (null === $this->channel) {
            $this->channel = $this->connection->channel();
            $this->channel->exchange_declare($this->exchange, 'fanout', false, true, false);
        }
        $this->channel->basic_publish(
            new AMQPMessage(
                \json_encode($task->input()->input()),
                [
                    'application_headers' => new AMQPTable(
                        [
                            'id' => $task->id()->id(),
                            'task' => $task->task()->task(),
                            'delay' => $delay->delay()
                        ]
                    )
                ]
            ),
            $this->exchange
        );
    }

    private function setExchange(string $exchange): void
    {
        $this->exchange = $exchange;
    }
}
