<?php

namespace Test\Timer;

class TaskName
{
    private $task;

    public function __construct(string $task)
    {
        $this->setTask($task);
    }

    public function task(): string
    {
        return $this->task;
    }

    private function setTask(string $task): void
    {
        $this->task = $task;
    }
}
