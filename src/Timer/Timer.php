<?php

namespace Test\Timer;

interface Timer
{
    public function schedule(TimerTask $task, Delay $delay);
}
