<?php

namespace Test\Timer;

class TaskInput
{
    private $input;

    public function __construct($input)
    {
        $this->setInput($input);
    }

    public function input()
    {
        return $this->input;
    }

    private function setInput($input): void
    {
        $this->input = $input;
    }
}
