<?php

namespace Test\Timer;

class TaskId
{
    private $id;

    public function __construct(string $id)
    {
        $this->setId($id);
    }

    public function id(): string
    {
        return $this->id;
    }

    private function setId(string $id): void
    {
        $this->id = $id;
    }
}
