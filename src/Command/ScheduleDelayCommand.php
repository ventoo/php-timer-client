<?php

namespace Test\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Test\Timer\Delay;
use Test\Timer\TaskId;
use Test\Timer\TaskInput;
use Test\Timer\TaskName;
use Test\Timer\Timer;
use Test\Timer\TimerTask;

class ScheduleDelayCommand extends Command
{
    private $timer;

    public function __construct(Timer $timer)
    {
        parent::__construct('test:schedule_delay');
        $this->timer = $timer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Test schedule command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->timer->schedule(
            new TimerTask(
                new TaskId(\bin2hex(random_bytes(16))),
                new TaskName('Test'),
                new TaskInput(null)
            ),
            new Delay(2000)
        );
    }
}
