<?php

namespace Test\Command;

use PhpAmqpLib\Connection\AbstractConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WaitForTaskCommand extends Command
{
    private $connection;

    public function __construct(AbstractConnection $connection)
    {
        parent::__construct('test:wait_for_task');
        $this->connection = $connection;
    }

    protected function configure()
    {
        $this
            ->setDescription('Wait for task execution')
            ->addArgument('task', InputArgument::REQUIRED, 'Task to be executed')
            ->addArgument('consumer', InputArgument::REQUIRED, 'Consumer name');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $task = $input->getArgument('task');
        $consumer = $input->getArgument('consumer');
        $channel = $this->connection->channel();
        $channel->exchange_declare($task, 'fanout', false, true, false);
        $channel->basic_qos(null, 100, null);
        $channel->queue_declare($consumer, false, true, false, false);
        $channel->queue_bind($consumer, $task);
        $channel->basic_consume(
            $consumer,
            '',
            false,
            false,
            false,
            false,
            function (AMQPMessage $message) {
                $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);
                echo \sprintf(
                    '[%s]    %s',
                    (new \DateTimeImmutable())->format('d/m/Y H:i:s.v'),
                    \json_decode($message->getBody(), true)
                ), PHP_EOL;
            }
        );
        while (\count($channel->callbacks)) {
            if ($this->connection->select(0, 150000)) {
                $channel->wait();
            }
        }
    }
}
