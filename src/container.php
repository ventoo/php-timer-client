<?php

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

$containerBuilder = new ContainerBuilder();
$loader = new YamlFileLoader($containerBuilder, new FileLocator(__DIR__));
$loader->load('./../services.yml');
$containerBuilder->setParameter('project_dir', __DIR__);
$containerBuilder->setParameter('var_dir', realpath(__DIR__ . '/../var'));
$containerBuilder->compile();

return $containerBuilder;
